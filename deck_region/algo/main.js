"use strict";

// Index 0 : region avec x terrain res.
// Index 1 : nb de region de ce type
let type_region = [[4,5],[3,8],[2,7],[1,2]];
let region_vide = [0,3];

let nb_ressource = [[20,'nourriture'],[12,'pierre'],[12,'bois'],[8,'minerais'],[4,'mana'],[4,'elevage']];

function nombre_terrain(table) {
  let resultat = 0;
  for (let i = 0; i < 4; i++) {
    resultat += table[i][0] * table[i][1];
  }
  return resultat;
}

console.log(nombre_terrain(type_region));

function nombre_region(table) {
  let resultat = 0;
  for (let i = 0; i < 4; i++) {
    resultat += table[i][1];
  }
  return resultat;
}

console.log(`${nombre_region(type_region)} + les ${region_vide[1]} terrains vide = ${nombre_region(type_region) + region_vide[1]}`);

function creation_table_terrain(ressource){
  let table_resultat = [];
  for (let i = 0; i < ressource.length; i++) {
    for (let y = 0; y < ressource[i][0]; y++) {
      table_resultat.push(ressource[i][1]);
    }
  }
  return table_resultat;
}

let total_terrain = creation_table_terrain(nb_ressource);

function repartition_terrain(table, ressource) {
  let table_resultat = [];
  for (let i = 0; i < table.length; i++) {
    table_resultat.push([]);
    for (let y = 0; y < table[i][1]; y++) {
      table_resultat[i].push([]);
      for (let u = 0; u < table[i][0]; u++) {
        let nb_alea = Math.floor(Math.random() * (ressource.length));
        table_resultat[i][y].push(ressource[nb_alea]);
        ressource.splice(nb_alea, 1);
      }
    }
  }
  return table_resultat;
}

let resultat = repartition_terrain(type_region, total_terrain);

console.log(resultat);